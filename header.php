<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" type="image/jpg" href="src/icons/logoBarraCafe.jpg">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css"
        integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://csshake.surge.sh/csshake.min.css">
    <link rel="stylesheet" href="css/style.css">

    <title>Restaurante BarraCafé</title>
</head>

<body>

    <header>
        <a href="/" role="button"><img src="src/icons/logoBarraCafe.png" alt="Logo barra café" width="80px"
                class="position-fixed top-0 start-0 mt-3 ms-3" style="z-index: 10"></a>
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner" style="height: 70vh">
                <?php for($i = 1; $i < 6; $i++) { ?>
                <div class="carousel-item position-relative <?php echo $t = $i == 1 ? 'active' : ''; ?>"
                    style="height: 100%">
                    <img src="src/slider/slide<?php echo $i; ?>.jpeg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block top-50 translate-middle-y text-shadow">
                        <h3 class="fw-bold h1">Restaurante <br> Barra café</h3>
                        <p class="fw-bold h2">Domicilios <br>
                            <span class="text-shadow-blue"><i class="fas fa-phone-alt"></i> (1)806 4773 <i
                                    class="fab fa-whatsapp"></i> 301 589 6576</span>
                        </p>
                    </div>
                </div>
                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
    </header>

    <hr class="mx-auto mt-4 bg-blue" style="width: 100px; height: 2px">

    <div class="container">