<?php require 'header.php';  ?>

    <div class="row">
        <div class="col"></div>
        <div class="col text-center">
            <a href="temporada.php" class="d-inline-block mx-auto">
                <div class="img-icon mx-auto shake-chunk shake-constant bell" role="button"></div>
            </a>
            <div class="text-center fw-bold lh-md">
                <p><a href="temporada.php" class="link-danger">Quédate en tu oficina <br>
                        Quédate en tu casa</a></p>
                <p class="text-secondary">Menú de temporada</p>
            </div>
        </div>
        <div class="col"></div>
    </div>
    <div class="row">
        <div class="col text-center">
            <a href="desayunos.php" class="d-inline-block">
                <div class="mx-auto img-icon breakfast"></div>
            </a>
            <p class="text-secondary fw-bold">Desayunos</p>
        </div>
        <div class="col text-center">
            <a href="lonchera.php" class="d-inline-block">
                <div class="mx-auto img-icon-static lonchera"></div>
            </a>
            <p class="text-secondary fw-bold">Lonchera</p>
        </div>
        <div class="col text-center">
            <a href="almuerzos.php" class="d-inline-block">
                <div class="mx-auto img-icon food"></div>
            </a>
            <p class="text-secondary fw-bold">Almuerzos</p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col text-center">
            <a href="tardeo.php" class="d-inline-block">
                <div class="mx-auto img-icon-static tardeo"></div>
            </a>
            <p class="text-secondary fw-bold">Tardeo</p>
        </div>
        <div class="col text-center">
            <a href="cena.php" class="d-inline-block">
                <div class="mx-auto img-icon dinner"></div>
            </a>
            <p class="text-secondary fw-bold">Cena</p>
        </div>
        <div class="col text-center">
            <a href="bar.php" class="d-inline-block">
                <div class="mx-auto img-icon-static bar"></div>
            </a>
            <p class="text-secondary fw-bold">Un buen <br>
                encuentro bar</p>
        </div>
    </div>


<?php require 'footer.php'; ?>