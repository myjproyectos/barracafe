<?php require 'header.php' ?>
<p class="h1 text-center text-blue fw-bold">Un buen encuentro bar</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Bebidas con licor"> Bebidas con licor
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Canelazo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Aguardiente</li>
                            <li class="list-group-item border-0">Ron</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Carajillo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Aguardiente</li>
                            <li class="list-group-item border-0">Ron</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Cervezas"> Cervezas
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Sabor Colombia
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Aguila</li>
                            <li class="list-group-item border-0">Andina</li>
                            <li class="list-group-item border-0">BBC</li>
                            <li class="list-group-item border-0">Club</li>
                            <li class="list-group-item border-0">Costeña</li>
                            <li class="list-group-item border-0">Poker</li>
                            <li class="list-group-item border-0">Redd’s</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Sabor global
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Azteca</li>
                            <li class="list-group-item border-0">Corona</li>
                            <li class="list-group-item border-0">Heineken</li>
                            <li class="list-group-item border-0">Miller</li>
                            <li class="list-group-item border-0">Stella artois</li>
                            <li class="list-group-item border-0">Tecate</li>
                        </ul>
                    </li>
                </ul>
                <p class="text-secondary h6 mt-2">* Solicitala michelada</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Cócteles"> Cócteles <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Con licor
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Cuba libre</li>
                            <li class="list-group-item border-0">Harmonia </li>
                            <li class="list-group-item border-0">Margarita</li>
                            <li class="list-group-item border-0">Mojito</li>
                            <li class="list-group-item border-0">Piña colada</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Sin licor
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Pasión invierno (soda + limón + hierbabuena)</li>
                            <li class="list-group-item border-0">Pasión primavera (soda + limón + maracuya)</li>
                            <li class="list-group-item border-0">Pasión verano (soda + limón + piña)</li>
                            <li class="list-group-item border-0">Pasión otoño (soda + limón + mango)</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Licores"> Licores
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Aguardiente </li>
                    <li class="list-group-item border-0">Ron </li>
                    <li class="list-group-item border-0">Vino </li>
                    <li class="list-group-item border-0">Vodka</li>
                    <li class="list-group-item border-0">Whisky</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php require 'bell.php';
require 'footer.php'?>