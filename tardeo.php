<?php require 'header.php'; ?>
<p class="h1 text-center text-blue fw-bold">Tardeo</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <div class="h2 text-center">Menú tardeo sal</div>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_arepa.png" alt="Arepas"> Arepas
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Carne desmechada salsa criolla</li>
                    <li class="list-group-item border-0">Cerdo desmechado salsa harmonia</li>
                    <li class="list-group-item border-0">Pollo desmechado salsa champiñones</li>
                    <li class="list-group-item border-0">Queso</li>
                    <li class="list-group-item border-0">Vegetariana</li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt=" Bebidas calientes"> Bebidas calientes
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Agua de panela con queso</li>
                    <li class="list-group-item border-0">Aromática de frutas</li>
                    <li class="list-group-item border-0">Café
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Americano</li>
                            <li class="list-group-item border-0">Capuchino</li>
                            <li class="list-group-item border-0">Capuchino licor</li>
                            <li class="list-group-item border-0">Express</li>
                            <li class="list-group-item border-0">Irlandes</li>
                            <li class="list-group-item border-0">Late</li>
                            <li class="list-group-item border-0">Moccachino</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Carajillo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Aguardiente</li>
                            <li class="list-group-item border-0">Ron</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Chocolate
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Milo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Te
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Bebidas frias "> Bebidas frias <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Agua</li>
                    <li class="list-group-item border-0">Energizantes</li>
                    <li class="list-group-item border-0">Gaseosas</li>
                    <li class="list-group-item border-0">Jugos naturales
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jufru (jugo con trozos de fruta)</li>
                    <li class="list-group-item border-0">Maltas</li>
                    <li class="list-group-item border-0">Malteadas</li>
                    <li class="list-group-item border-0">Milo leche</li>
                    <li class="list-group-item border-0">Smothie
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">3 frutas
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Agua</li>
                                    <li class="list-group-item border-0">Leche</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Frutas y verduras</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Te</li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_canasta_platano.png" alt="Canastas de plátano"> Canastas de
                    plátano
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Canasta con
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne ropa vieja</li>
                            <li class="list-group-item border-0">Cerdo salsa harmonia</li>
                            <li class="list-group-item border-0">Pollo champiñón</li>
                            <li class="list-group-item border-0">Queso bocadillo</li>
                            <li class="list-group-item border-0">Vegetariano</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_empanadas.png" alt="Empanadas "> Empanadas
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Carne</li>
                    <li class="list-group-item border-0">Pollo</li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_mazorca.png" alt="Mazorca desgranada "> Mazorca desgranada <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Mazorcada
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Harmonia (maiz + 3 carnes + queso + papita rayada +
                                salsa harmonia)</li>
                            <li class="list-group-item border-0">Maíz + carne + queso + papita rayada + salsa harmonia
                            </li>
                            <li class="list-group-item border-0">Maíz +cerdo + queso + papita rayada + salsa harmonia
                            </li>
                            <li class="list-group-item border-0">Maíz + pollo + queso + papita rayada + salsa harmonia
                            </li>
                            <li class="list-group-item border-0">Vegetariana (maíz + champiñón + queso + papita rayada +
                                salsa harmonia)</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_papa.png" alt="Papa francesa"> Papa francesa
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Papa
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chorillana (carne en cubitos + cebolla frita)</li>
                            <li class="list-group-item border-0">Choripapa</li>
                            <li class="list-group-item border-0">Pacarne</li>
                            <li class="list-group-item border-0">Papa francesa</li>
                            <li class="list-group-item border-0">Papollo</li>
                            <li class="list-group-item border-0">Queso gratinado</li>
                            <li class="list-group-item border-0">Salchipapa</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_picada.png" alt="Picadas "> Picadas
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Picada
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne res + cerdo + pollo + papa francesa + salchicha
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_pinchos.png" alt="Pinchos"> Pinchos
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Pinchos
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Bocadillo y queso</li>
                            <li class="list-group-item border-0">Cerdo</li>
                            <li class="list-group-item border-0">Chorizo</li>
                            <li class="list-group-item border-0">Mixto</li>
                            <li class="list-group-item border-0">Pollo</li>
                            <li class="list-group-item border-0">Res</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_sanduches.png" alt="Sanduches "> Sanduches
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Sanduche
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">Pollo</li>
                            <li class="list-group-item border-0">Vegetariano</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_tortillas.png" alt="Tortillas "> Tortillas
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Tortilla
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Bogotana (champiñon + maíz + papa)</li>
                            <li class="list-group-item border-0">Boyacense (queso + papa + tomate)</li>
                            <li class="list-group-item border-0">Eje cafetero (chorizo + papa)</li>
                            <li class="list-group-item border-0">Española (cebolla + papa)</li>
                            <li class="list-group-item border-0">Harmonia (carne + papa + pollo)</li>
                            <li class="list-group-item border-0">Huilense (maíz + papa + salchicha)</li>
                            <li class="list-group-item border-0">Llanera (papa + plátano)</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="h2 text-center">Menú tardeo dulce</div>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-solar.png" alt="Colaciones "> Colaciones <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Hojaldre
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Corazón</li>
                            <li class="list-group-item border-0">Gloria</li>
                            <li class="list-group-item border-0">Pasaboca</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate">
                    <img width="40px" src="src/icons/combo-solar.png" alt="Malteadas"> Malteadas <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                </div>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-solar.png" alt="Postres"> Postres <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Brownie
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Brownie caliente</li>
                            <li class="list-group-item border-0">Brownie con helado</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Milhoja
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Arequipe</li>
                            <li class="list-group-item border-0">Fondane</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Tentación de chocolate</li>
                </ul>
                <p class="text-secondary h6 mt-2">* Opción copa de helado</p>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-solar.png" alt="Tortas "> Tortas <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Casera</li>
                    <li class="list-group-item border-0">Manzana</li>
                    <li class="list-group-item border-0">Naranja</li>
                    <li class="list-group-item border-0">Negro tradicional</li>
                    <li class="list-group-item border-0">Zanahoria</li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-solar.png" alt="Waffles"> Waffles <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Invierno (Base arequipe ó Hersheys ó nutela ó syrup + helado)
                    </li>
                    <li class="list-group-item border-0">Otoño (Base arequipe ó Hersheys ó nutela ó syrup + fresa)</li>
                    <li class="list-group-item border-0">Primavera (Base arequipe ó Hersheys ó nutela ó syrup + durazno
                        ó mango)</li>
                    <li class="list-group-item border-0">Verano (Base arequipe ó Hersheys ó nutela ó syrup + banano)
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php require 'bell.php'; 
require 'footer.php'; ?>