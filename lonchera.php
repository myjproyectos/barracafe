<?php require 'header.php' ?>
<p class="h1 text-center text-blue fw-bold">Lonchera</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Bebidas calientes" class="me-2">Bebidas
                    calientes <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2"
                        role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Agua de panela con queso</li>
                    <li class="list-group-item border-0">Aromática de frutas</li>
                    <li class="list-group-item border-0">Café americano</li>
                    <li class="list-group-item border-0">Café capuchino</li>
                    <li class="list-group-item border-0">Café capuchino licor</li>
                    <li class="list-group-item border-0">Café express</li>
                    <li class="list-group-item border-0">Café irlandés</li>
                    <li class="list-group-item border-0">Café late</li>
                    <li class="list-group-item border-0">Café moccachino</li>
                    <li class="list-group-item border-0">Carajillo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Aguardiente</li>
                            <li class="list-group-item border-0">Ron</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Chocolate
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Milo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Té
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Bebidas frias " class="me-2">Bebidas frias <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Agua</li>
                    <li class="list-group-item border-0">Energizantes</li>
                    <li class="list-group-item border-0">Gaseosas</li>
                    <li class="list-group-item border-0">Jugos naturales
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Agua</li>
                            <li class="list-group-item border-0">Leche</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jufru (Jugo con trozos de fruta)</li>
                    <li class="list-group-item border-0">Maltas</li>
                    <li class="list-group-item border-0">Malteadas</li>
                    <li class="list-group-item border-0">Milo leche</li>
                    <li class="list-group-item border-0">Smoothie
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">3 frutas
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Agua</li>
                                    <li class="list-group-item border-0">Leche</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Frutas y verduras</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Té</li>
                </ul>
            </div>
        </div>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_pinchos.png" alt="Pinchos " class="me-2">Pinchos <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Bocadillo y queso</li>
                    <li class="list-group-item border-0">Cerdo</li>
                    <li class="list-group-item border-0">Chorizo</li>
                    <li class="list-group-item border-0">Pollo</li>
                    <li class="list-group-item border-0">Res</li>
                    <li class="list-group-item border-0">Mixto</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Porción fruta " class="me-2">Porción fruta <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Con queso rallado</li>
                    <li class="list-group-item border-0">Mixta</li>
                    <li class="list-group-item border-0">Salpicón</li>
                    <li class="list-group-item border-0">Sencilla</li>
                </ul>
            </div>
        </div>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_sanduches.png" alt="Sanduches" class="me-2">Sanduches <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Jamón y queso</li>
                    <li class="list-group-item border-0">Pollo </li>
                    <li class="list-group-item border-0">Vegetariano</li>
                </ul>
            </div>
        </div>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Snack’s" class="me-2">Snack’s <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Arepa
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne desmechada salsa criolla</li>
                            <li class="list-group-item border-0">Cerdo desmechado salsa harmonía</li>
                            <li class="list-group-item border-0">Pollo desmenuzado salsa champiñones</li>
                            <li class="list-group-item border-0">Queso</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Empanadas
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne</li>
                            <li class="list-group-item border-0">Pollo</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php require 'bell.php'; 
require 'footer.php'; ?>