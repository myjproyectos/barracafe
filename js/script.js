var myCarousel = document.querySelector('#carouselExampleCaptions')
var carousel = new bootstrap.Carousel(myCarousel)

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})


$(document).ready(function(){
    $(".h4, .h5, .h6").hide()
    
    $(".h3 .fate").on('click', function(){
        eventoClick($(this), ".h4")
    })

    $(".h4 .fate2").on('click', function(){
        eventoClick($(this), ".h5")
    })

    function eventoClick(o, c){
        var e = o.children(".fa-chevron-right")
        o.siblings(c).slideToggle()
        if(e.hasClass('rotate')){
            o.siblings(".h6").hide()
            e.removeClass('rotate')
        }else{
            o.siblings(".h6").show()
            e.addClass('rotate')
        }
    }
})