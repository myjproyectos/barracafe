<?php require 'header.php'; ?>
<p class="h1 text-center text-blue fw-bold">Menú de temporada</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <div class="list-group">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/adelanta-cuaderno.png" alt="Desayuno" class="me-2"> Desayuno <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <div class="list-group mt-3 h4">
                    <div class="list-group-item list-group-item-action position-relative border-0" role="button">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/combo-solar.png" alt="Solar" class="me-2"> Solar <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group h5 mt-3">
                            <li class="list-group-item border-0">Fruta</li>
                            <li class="list-group-item border-0">Sanduche
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Jamon y queso</li>
                                    <li class="list-group-item border-0">Pollo</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Bebida caliente
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Aromática</li>
                                    <li class="list-group-item border-0">Café</li>
                                    <li class="list-group-item border-0">Chocolate</li>
                                </ul>
                            </li>
                        </ul>
                        <p class="text-secondary h6 mt-2">* Opción jugo de naranja o zanahoria</p>
                    </div>
                    <div class="list-group-item list-group-item-action position-relative border-0" role="button">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/tu-combo.png" alt="Quimbaya" class="me-2"> Quimbaya <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group mt-3 h5">
                            <li class="list-group-item border-0">Huevos al gusto
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Fritos</li>
                                    <li class="list-group-item border-0">Revueltos</li>
                                    <li class="list-group-item border-0">Perico</li>
                                    <li class="list-group-item border-0">Salchicha</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Opción adición arroz</li>
                            <li class="list-group-item border-0">Arepa de la casa o pan</li>
                            <li class="list-group-item border-0">Bebida caliente
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Aromática</li>
                                    <li class="list-group-item border-0">Café</li>
                                    <li class="list-group-item border-0">Chocolate</li>
                                </ul>
                            </li>
                        </ul>
                        <p class="text-secondary h6 mt-2">* Opción jugo de naranja o zanahoria</p>
                    </div>
                    <div class="list-group-item list-group-item-action position-relative border-0" role="button">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/tu-combo.png" alt="San Agustín" class="me-2"> San Agustín <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group mt-3 h5">
                            <li class="list-group-item border-0">Fruta</li>
                            <li class="list-group-item border-0">Huevos al gusto
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Fritos</li>
                                    <li class="list-group-item border-0">Pericos</li>
                                    <li class="list-group-item border-0">Revueltos</li>
                                    <li class="list-group-item border-0">Salchicha</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Opción adición de arroz</li>
                            <li class="list-group-item border-0">Arepa de la casa o pan</li>
                            <li class="list-group-item border-0">Bebida caliente
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Aromática</li>
                                    <li class="list-group-item border-0">Café</li>
                                    <li class="list-group-item border-0">Chocolate</li>
                                </ul>
                            </li>
                        </ul>
                        <p class="text-secondary h6 mt-2">* Opción jugo de naranja o zanahoria</p>
                    </div>
                    <div class="list-group-item list-group-item-action position-relative border-0" role="button">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/tu-combo.png" alt="Muisca" class="me-2"> Muisca <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group mt-3 h5">
                            <li class="list-group-item border-0">Fruta</li>
                            <li class="list-group-item border-0">Huevos al gusto
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Fritos</li>
                                    <li class="list-group-item border-0">Pericos</li>
                                    <li class="list-group-item border-0">Revueltos</li>
                                    <li class="list-group-item border-0">Salchicha</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Opción adición de arroz</li>
                            <li class="list-group-item border-0">Caldo
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Carne</li>
                                    <li class="list-group-item border-0">Pollo</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Arepa de la casa o pan</li>
                            <li class="list-group-item border-0">Bebida caliente
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Aromática </li>
                                    <li class="list-group-item border-0">Café </li>
                                    <li class="list-group-item border-0">Chocolate</li>
                                </ul>
                            </li>
                        </ul>
                        <p class="text-secondary h6 mt-2">* Opción jugo de naranja o zanahoria</p>
                    </div>
                    <div class="list-group-item list-group-item-action position-relative border-0" role="button">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/tu-combo.png" alt="Eclipse" class="me-2"> Eclipse <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group mt-3 h5">
                            <li class="list-group-item border-0">Caldo
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Carne</li>
                                    <li class="list-group-item border-0">Mixto (carne + pollo)</li>
                                    <li class="list-group-item border-0">Pollo</li>
                                    <li class="list-group-item border-0">Opción huevo dentro del caldo</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Huevos al gusto
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Fritos</li>
                                    <li class="list-group-item border-0">Jamón y queso</li>
                                    <li class="list-group-item border-0">Pericos</li>
                                    <li class="list-group-item border-0">Revueltos</li>
                                    <li class="list-group-item border-0">Salchicha</li>
                                    <li class="list-group-item border-0">Opción adición de arroz</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Arepa de la casa o pan</li>
                            <li class="list-group-item border-0">Bebida caliente
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Aromática</li>
                                    <li class="list-group-item border-0">Café </li>
                                    <li class="list-group-item border-0">Chocolate</li>
                                </ul>
                            </li>
                            <li class="list-group-item border-0">Ó Jugo
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">Naranja</li>
                                    <li class="list-group-item border-0">Zanahoria</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <img width="40px" src="src/icons/adelanta-cuaderno.png" alt="Desayuno" class="me-2"> <a
                    href="lonchera.php" class="link-danger text-decoration-none">Lonchera</a>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/adelanta-cuaderno.png" alt="Desayuno" class="me-2"> Almuerzo <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <div class="list-group mt-3 h4">
                    <div class="list-group-item list-group-item-action position-relative border-0">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/tu-combo.png" alt="Eclipse" class="me-2"> Menú del día <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group mt-3 h5">
                            <li class="list-group-item border-0">Todos los días un menú diferente</li>
                        </ul>
                        <p class="text-secondary h6 mt-2">* Incluye: sopa o crema o fruta, limonada o jugo del día </p>
                    </div>
                    <div class="list-group-item list-group-item-action position-relative border-0">
                        <div class="fate2" role="button">
                            <img width="40px" src="src/icons/tu-combo.png" alt="Eclipse" class="me-2"> Menú de antaño <i
                                class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                        </div>
                        <ul class="list-group h5 mt-3">
                            <li class="list-group-item border-0">Ajiaco santafereño</li>
                            <li class="list-group-item border-0">Arroz con pollo</li>
                            <li class="list-group-item border-0">Cazuela de lenteja</li>
                            <li class="list-group-item border-0">Chuleta valluna</li>
                            <li class="list-group-item border-0">Cordon blue</li>
                            <li class="list-group-item border-0">Filete de pollo plancha</li>
                            <li class="list-group-item border-0">Frijolada</li>
                            <li class="list-group-item border-0">Goulash (cerdo o pollo o res o mixto)</li>
                            <li class="list-group-item border-0">Lasaña</li>
                            <li class="list-group-item border-0">Parrilla (carne cerdo o res)</li>
                            <li class="list-group-item border-0">Tabla mixta (cerdo + res + pollo)</li>
                        </ul>
                        <p class="text-secondary h6 mt-2">* Todos los menus incluyen entre 1 a 3 complementos</p>
                        <p class="text-secondary h6">** Todos los menus incluyen bebida, limonada o jugo día</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
            <img width="40px" src="src/icons/recarga_baterias.png" alt="Desayuno" class="me-2"> <a href="tardeo.php"
                class="link-danger text-decoration-none">Tardeo</a>
        </div>
        <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
            <img width="40px" src="src/icons/adelanta-cuaderno.png" alt="Desayuno" class="me-2"> <a href="cena.php"
                class="link-danger text-decoration-none">Cena</a>
        </div>
        <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
            <img width="40px" src="src/icons/buen_encuentro.png" alt="Desayuno" class="me-2"> <a href="bar.php"
                class="link-danger text-decoration-none">Bar</a>
        </div>
    </div>
</div>
<?php
    require 'bell.php';
    require 'footer.php';
?>