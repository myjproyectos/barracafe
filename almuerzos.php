<?php require 'header.php'; ?>
<p class="h1 text-center text-blue fw-bold">Almuerzos</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Menus de antaño" class="me-2"> Menus de antaño
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Ajiaco con pollo</li>
                    <li class="list-group-item border-0">Lasaña mixta</li>
                </ul>
                <p class="text-secondary h6 mt-2">* Menú incluye limonada natural o jugo del día</p>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Menus de carta" class="me-2"> Menus de carta <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2"></i>
                </div>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Bistec a caballo</li>
                    <li class="list-group-item border-0">Carne especial
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Champiñones</li>
                            <li class="list-group-item border-0">Encebollada</li>
                            <li class="list-group-item border-0">Gratinada</li>
                            <li class="list-group-item border-0">Plancha</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Carne + arepa</li>
                    <li class="list-group-item border-0">Cerdo + arepa</li>
                    <li class="list-group-item border-0">Chuleta valluna</li>
                    <li class="list-group-item border-0">Churrasco</li>
                    <li class="list-group-item border-0">Lomo de cerdo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">A caballo</li>
                            <li class="list-group-item border-0">Champiñon</li>
                            <li class="list-group-item border-0">Encebollado</li>
                            <li class="list-group-item border-0">Gratinado</li>
                            <li class="list-group-item border-0">Plancha</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Pechuga especial
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">A caballo</li>
                            <li class="list-group-item border-0">Champiñón</li>
                            <li class="list-group-item border-0">Encebollada</li>
                            <li class="list-group-item border-0">Gratinada</li>
                            <li class="list-group-item border-0">Plancha</li>
                            <li class="list-group-item border-0">Pomodoro</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Pechuga + arepa</li>
                    <li class="list-group-item border-0">Punta de anca de cerdo</li>
                    <li class="list-group-item border-0">Tabla mixta2 (su opción)</li>
                    <li class="list-group-item border-0">3 (carne + cerdo + pollo)</li>
                </ul>
                <p class="text-secondary h6 mt-2">* Menú incluye limonada natural o jugo del día <br>
                    ** Los completos por menú pueden variar</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Menus del día " class="me-2"> Menus del día <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i>
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2"></i>
                </div>
                <p class="text-secondary h6 mt-2">Todos los dias 4 menus diferentes con sabor de casa y recetas de la
                    abuela.</p>
                <ul class="list-group mt-3 h4 h5">
                    <li class="list-group-item border-0">Arroces</li>
                    <li class="list-group-item border-0">Burritos</li>
                    <li class="list-group-item border-0">Canelones</li>
                    <li class="list-group-item border-0">Carne de res</li>
                    <li class="list-group-item border-0">Cazuelas</li>
                    <li class="list-group-item border-0">Cerdo</li>
                    <li class="list-group-item border-0">Crepes</li>
                    <li class="list-group-item border-0">Higado</li>
                    <li class="list-group-item border-0">Mazorcadas</li>
                    <li class="list-group-item border-0">Pescados</li>
                    <li class="list-group-item border-0">Pollos</li>
                    <li class="list-group-item border-0">Spaghetti</li>
                    <li class="list-group-item border-0">Wraps</li>
                </ul>
                <p class="text-secondary h6 mt-2">* Todos los menus tienes entre 3 y 4 complementos que cambian todos
                    los dias <br>
                    ** Menu incluye limonada natural o jugo del día <br>
                    *** Menus, menos cazuela, incluye crema, fruta o sopa.</p>
            </div>
        </div>
    </div>
</div>
<?php require 'bell.php';
require 'footer.php'; ?>