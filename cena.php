<?php require 'header.php' ?>
<p class="h1 text-center text-blue fw-bold">Cena</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Cerdo "> Cerdo
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Chuleta valluna</li>
                    <li class="list-group-item border-0">Lomo de cerdo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Caballo</li>
                            <li class="list-group-item border-0">Champiñones</li>
                            <li class="list-group-item border-0">Encebollado</li>
                            <li class="list-group-item border-0">Gratinado</li>
                            <li class="list-group-item border-0">Plancha</li>
                            <li class="list-group-item border-0">Punta de anca cerdo</li>
                        </ul>
                    </li>
                </ul>
                <p class="text-secondary h6 mt-2">* Menú incluye 3 complementos</p>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_lasagna.png" alt="Lasaña"> Lasaña
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Lasaña
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Pasta mixta (carne y pollo)</li>
                            <li class="list-group-item border-0">Pasta mixta mini (carne + pollo)</li>
                            <li class="list-group-item border-0">Platano mixta (carne + pollo)</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Pollo"> Pollo
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Pechuga especial
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">A caballo</li>
                            <li class="list-group-item border-0">Champiñones</li>
                            <li class="list-group-item border-0">Encebollada</li>
                            <li class="list-group-item border-0">Gratinada</li>
                            <li class="list-group-item border-0">Plancha</li>
                            <li class="list-group-item border-0">Pomadero</li>
                        </ul>
                    </li>
                </ul>
                <p class="text-secondary h6 mt-2">* Menú incluye 3 complementos</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_carne.png" alt="Res"> Res
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Bistec a caballo</li>
                    <li class="list-group-item border-0">Carne especial
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Champiñones</li>
                            <li class="list-group-item border-0">Encebollada</li>
                            <li class="list-group-item border-0">Gratinada</li>
                            <li class="list-group-item border-0">Plancha</li>
                            <li class="list-group-item border-0">Churrasco</li>
                        </ul>
                    </li>
                </ul>
                <p class="text-secondary h6 mt-2">* Menú incluye 3 complementos</p>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/sal_sopa.png" alt="Sopa "> Sopa
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Ajiaco
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Ajiaco completo</li>
                            <li class="list-group-item border-0">Ajiaco media porción</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Tabla mixta"> Tabla mixta
                    <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Tabla mixta
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">2 carnes (su opción)</li>
                            <li class="list-group-item border-0">3 carnes (carne + cerdo + pollo)</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php require 'bell.php';
require 'footer.php'; ?>