<?php require 'header.php' ?>
<p class="h1 text-center text-blue fw-bold">Desayunos</p>
<p class="h2 text-center mt-2 fw-bold">Pídelo en:</p>
<div class="row mt-5 mb-5">
    <div class="col">
        <p class="h2 text-center">Combos</p>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-solar.png" alt="Solar" class="me-2"> Solar <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 mt-3 h5">
                    <li class="list-group-item border-0">Fruta</li>
                    <li class="list-group-item border-0">Sanduche
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Jamon y queso</li>
                            <li class="list-group-item border-0">Pollo</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Aromática</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Chocolate</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-ambrosía.png" alt="Ambrosía" class="me-2"> Ambrosía <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Cereal</li>
                    <li class="list-group-item border-0">Fruta</li>
                    <li class="list-group-item border-0">Yogur</li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">(Opción adición jugo)</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-lunar.png" alt="Lunar" class="me-2"> Lunar <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Pancake</li>
                    <li class="list-group-item border-0">Fruta</li>
                    <li class="list-group-item border-0">Huevos al gusto
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Fritos</li>
                            <li class="list-group-item border-0">Revueltos</li>
                            <li class="list-group-item border-0">Pericos</li>
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">Salchicha</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Agua aromática</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Barra café" class="me-2"> Barra café <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Fruta</li>
                    <li class="list-group-item border-0">Omelette
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Pollo</li>
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">Champiñón y espinaca</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">(Opción adición jugo)</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-lunar.png" alt="Eclipse " class="me-2"> Eclipse <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Caldo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne</li>
                            <li class="list-group-item border-0">Pollo</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Huevos al gusto
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Fritos</li>
                            <li class="list-group-item border-0">Salchicha</li>
                            <li class="list-group-item border-0">Pericos</li>
                            <li class="list-group-item border-0">Revueltos</li>
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">(Opción porción de arroz)</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Arepa de la casa o pan</li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">(Opción adición jugo)</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt="Harmonía " class="me-2"> Harmonía <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Calentao + huevo</li>
                    <li class="list-group-item border-0">Arepa de la casa o pan</li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-solsticio.png" alt="Solsticio" class="me-2"> Solsticio <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Carne o pollo en bistec</li>
                    <li class="list-group-item border-0">Papa sudada</li>
                    <li class="list-group-item border-0">Arroz</li>
                    <li class="list-group-item border-0">Huevos al gusto
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Fritos</li>
                            <li class="list-group-item border-0">Pericos</li>
                            <li class="list-group-item border-0">Revueltos</li>
                            <li class="list-group-item border-0">Mazorca</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/combo-equinoccio.png" alt="Equinoccio" class="me-2"> Equinoccio <i
                        class="fas fa-exclamation-triangle position-absolute end-0 mt-2 me-5 text-warning"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="No disponible"></i> <i
                        class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2" role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Calentao + huevo</li>
                    <li class="list-group-item border-0">Arepa de la casa o pan</li>
                    <li class="list-group-item border-0">Caldo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne</li>
                            <li class="list-group-item border-0">Pollo</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria
                                <ul class="list-group mt-3">
                                    <li class="list-group-item border-0">(Opción adición jugo)</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col">
        <p class="h2 text-center">Carta</p>
        <div class="list-group mt-3">
            <div class="list-group-item list-group-item-action position-relative border-0 h3" role="button">
                <div class="fate" role="button">
                    <img width="40px" src="src/icons/tu-combo.png" alt=" Inspiraciones para ti" class="me-2">
                    Inspiraciones para ti <i class="fas fa-chevron-right position-absolute end-0 mt-2 text-blue me-2"
                        role="button"></i>
                </div>
                <ul class="list-group h4 h5 mt-3">
                    <li class="list-group-item border-0">Arepa de la casa</li>
                    <li class="list-group-item border-0">Arroz (porción)</li>
                    <li class="list-group-item border-0">Bebida caliente
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Chocolate</li>
                            <li class="list-group-item border-0">Café</li>
                            <li class="list-group-item border-0">Aromática</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Caldo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne</li>
                            <li class="list-group-item border-0">Pollo</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Calentao</li>
                    <li class="list-group-item border-0">Carne en bistec</li>
                    <li class="list-group-item border-0">Cereal</li>
                    <li class="list-group-item border-0">Fruta (porción)</li>
                    <li class="list-group-item border-0">Huevos
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Fritos</li>
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">Mazorca</li>
                            <li class="list-group-item border-0">Pericos</li>
                            <li class="list-group-item border-0">Revueltos</li>
                            <li class="list-group-item border-0">Salchicha</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Naranja</li>
                            <li class="list-group-item border-0">Zanahoria</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Jugo en leche</li>
                    <li class="list-group-item border-0">Omelette
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Champiñón y espinaca</li>
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">Pollo</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Pancake</li>
                    <li class="list-group-item border-0">Pollo en bistec</li>
                    <li class="list-group-item border-0">Sandwich
                        <ul class="list-group mt-3">
                            <li class="list-group-item border-0">Carne desmechada</li>
                            <li class="list-group-item border-0">Jamón y queso</li>
                            <li class="list-group-item border-0">Pollo </li>
                            <li class="list-group-item border-0">Vegetariano</li>
                        </ul>
                    </li>
                    <li class="list-group-item border-0">Yogur</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php require 'bell.php';
require 'footer.php' ?>